//
//  ViewController.swift
//  SampleLogin
//
//  Created by Etsushi Otani on 2021/08/16.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit

class ViewController: UIViewController {
    
    @IBOutlet weak var customLoginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let loginButton = FBLoginButton()
        loginButton.center = view.center
        loginButton.delegate = self
        view.addSubview(loginButton)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkToken()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        buttonLayout()
    }
    
    private func buttonLayout() {
        customLoginButton.setImage(UIImage(named: "f_logo"), for: .normal)
        customLoginButton.imageView?.contentMode = .scaleAspectFit
        customLoginButton.contentHorizontalAlignment = .center
        customLoginButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        customLoginButton.layer.cornerRadius = 25
        customLoginButton.layer.borderColor = UIColor.black.cgColor
        customLoginButton.layer.borderWidth = 2
    }
    
    private func checkToken() {
        if let token = AccessToken.current, !token.isExpired {
            print("Access token: " + token.tokenString)
        }
        
    }
    
    @IBAction func tappedCustomButton(_ sender: Any) {
        LoginManager().logIn(permissions: [.publicProfile, .email], viewController: self, completion: { result in
            switch result {
            case let .success(granted: _, declined: _, token: token):
                guard let token = token else {
                    return
                }
                print(token.tokenString)
            case let .failed(error):
                print(error.localizedDescription)
            case .cancelled:
                print("##### cancelled ######")
            }
        })
    }
}

extension ViewController: LoginButtonDelegate {
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        if error == nil {
            //ログイン成功時の処理、tokenをサーバに送ったりする
            guard let token = result?.token else {
                return
            }
            print("#### login success ####")
            print("Access token: " + token.tokenString)
        } else {
            //失敗時の処理
            print(error.debugDescription)
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        //ログアウト直後の処理
        print("#### logout ####")
    }
}

